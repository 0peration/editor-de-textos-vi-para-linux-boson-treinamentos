# Editor de textos vi para Linux - Bóson Treinamentos

Como usar o Editor de Textos Vi / Vim (Visual Editor) no Linux
Neste vídeo apresentamos nosso Mini Curso do editor de textos vi no Linux.
Este vídeo é parte integrante do Curso Preparatório para Certificação Linux LPIC-1 e do Curso de Linux Básico.
## Editor de textos vi para Linux
    * https://www.youtube.com/watch?v=bXdX5X3JoI8&list=PLucm8g_ezqNrSiAV5kYSW4h_xrC4dWVRh
    * https://www.notion.so/Editor-de-textos-vi-para-Linux-1209e7a0d38841529997ab4ff99a7016

## Link da documentação do VI
    * http://vimdoc.sourceforge.net/

## Instalação do GVim
    * GVim é uma inteface visual do Vi pode fazer toda as operações que fazemos no VI com linha de comandos.
        Comando para instalar o GVim no *Ubuntu
        - sudo apt install vim-gtk3
        - para rodar colocar gvim no terminal e da enter que vai par o modo grafico.

## Mais links
    * https://www.linuxforce.com.br/comandos-linux/comandos-linux-comando-vi/
    * https://receitasdecodigo.com.br/ubuntu/usando-vi-no-linux
    * https://br.ccm.net/contents/322-linux-o-editor-vi
    * https://www.vivaolinux.com.br/dica/VI-O-fantastico-editor-de-textos
    * https://guialinux.uniriotec.br/vim/

## Certificação Linux 
    * http://linuxsemfronteiras.com.br/certificacoes/
    * https://www.lpi.org/pt/
